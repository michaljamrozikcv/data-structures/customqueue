package com.example.queue;

public class CustomQueue<T> {
    private int size;
    private Node last;

    public int getSize() {
        return size;
    }

    /**
     * Add object at the end of a queue
     *
     * @param item object to be added
     */
    public void add(T item) {
        Node node = new Node(item);
        node.previous = last;
        last = node;
        size++;
    }

    /**
     * Remove object from queue; object removed is always first item in a queue
     */
    public T remove() {
        Node removed = last;
        if (size == 0) {
            return null;
        } else if (size == 1) {
            size--;
            last = null;
        } else {
            Node temp = last;
            while (temp.previous.previous != null) {
                temp = temp.previous;
            }
            removed = temp.previous;
            temp.previous = null;
            size--;
        }
        return removed.item;
    }

    /**
     * Check if specific object is already in a queue
     *
     * @param item object to be checked if already added to the queue
     */
    public boolean contains(T item) {
        if (size == 0) {
            return false;
        }
        Node temp = last;
        while (temp != null) {
            if (temp.item == item) {
                return true;
            }
            temp = temp.previous;
        }
        return false;
    }

    /**
     * Get a peek of a queue (first item in queue; is user want to remove object, peek will be deleted first)
     */
    public T peek() {
        Node temp = last;
        if (size == 0) {
            return null;
        }
        while (temp.previous != null) {
            temp = temp.previous;
        }
        return temp.item;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Queue [");
        if (last == null) {
            return result.append("] size: ").append(" size: " + size).toString();
        } else {
            Node temp = last;
            while (temp.previous != null) {
                result.append(temp).append(" --> ");
                temp = temp.previous;
            }
            result.append(temp);
        }
        return result + "]" + " size: " + size;
    }

    private class Node {
        private final T item;
        private Node previous;

        private Node(T item) {
            this.item = item;
        }

        @Override
        public String toString() {
            return item.toString();
        }
    }
}
