package com.example.queue;

import com.example.queue.elements.Car;
import com.example.queue.elements.Person;

public class Main {
    public static void main(String[] args) {
        queueOfPeople();
        queueOfCars();
    }

    private static void queueOfCars() {
        System.out.println("\n----------------------------------------------------------------------");
        System.out.println("QUEUE OF CARS");
        System.out.println("------------------------------------------------------------------------");
        Car c1 = new Car("model1", "vin1");
        Car c2 = new Car("model2", "vin2");
        Car c3 = new Car("model3", "vin3");
        Car c4 = new Car("model4", "vin4");
        CustomQueue<Car> queueOfCars = new CustomQueue<>();
        System.out.println("\n----------------------Adding to queue-------------------");
        queueOfCars.add(c1);
        queueOfCars.add(c2);
        queueOfCars.add(c3);
        System.out.println(queueOfCars + "\n");
        System.out.println("-----------------------------Peek------------------------------");
        System.out.println("Peek: " + queueOfCars.peek() + "\n");
        System.out.println("------------------------Removing ------------------------");
        for (int i = 0; i < 4; i++) {
            if (queueOfCars.getSize() == 0) {
                System.out.println("Cannot remove, queue is already empty\n");
            } else {
                queueOfCars.remove();
                System.out.println("Item is leaving the queue....");
                System.out.println(queueOfCars + "\n");
            }
        }
        System.out.println("-----------------------Contains-------------------------------");
        queueOfCars.add(c4);
        System.out.println(queueOfCars + "\n");
        System.out.println("Does queue contains: " + c4 + "--> " + queueOfCars.contains(c4));
        System.out.println("Does queue contains: " + c3 + "--> " + queueOfCars.contains(c3));
    }

    private static void queueOfPeople() {
        System.out.println("\n------------------------------------------------------------------------");
        System.out.println("QUEUE OF PEOPLE");
        System.out.println("------------------------------------------------------------------------");
        Person p1 = new Person("name1", "surname1");
        Person p2 = new Person("name2", "surname2");
        Person p3 = new Person("name3", "surname3");
        Person p4 = new Person("name4", "surname4");
        CustomQueue<Person> queueOfPeople = new CustomQueue<>();
        System.out.println("\n----------------------Adding to queue-------------------");
        queueOfPeople.add(p1);
        queueOfPeople.add(p2);
        queueOfPeople.add(p3);
        System.out.println(queueOfPeople + "\n");
        System.out.println("-----------------------------Peek------------------------------");
        System.out.println("Peek: " + queueOfPeople.peek() + "\n");
        System.out.println("------------------------Removing ------------------------");
        for (int i = 0; i < 4; i++) {
            if (queueOfPeople.getSize() == 0) {
                System.out.println("Cannot remove, queue is already empty\n");
            } else {
                queueOfPeople.remove();
                System.out.println("Item is leaving the queue....");
                System.out.println(queueOfPeople + "\n");
            }
        }
        System.out.println("-----------------------Contains-------------------------------");
        queueOfPeople.add(p4);
        System.out.println(queueOfPeople + "\n");
        System.out.println("Does queue contains: " + p4 + "--> " + queueOfPeople.contains(p4));
        System.out.println("Does queue contains: " + p2 + "--> " + queueOfPeople.contains(p2));
    }
}

